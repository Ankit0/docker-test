const express = require("express");
const app = express();

const PORT = process.env.PORT || 3000;

app.get("/", (req,res) => {
    console.log('homepage called');
    res.status(200).json({success : true, data: "This is home path"})
})

app.get("/test", (req,res) => {
    console.log('test called');
    res.status(200).json({success : true, data: "This is test path"})
})

app.listen(PORT, () => {
    console.log(` app running on port ${PORT}`);
})